package com.chilldudes.buyall.activities;


import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import com.chilldudes.buyall.MainActivity;
import com.chilldudes.buyall.R;
import com.chilldudes.buyall.adapters.IntroViewPagerAdapter;
import com.chilldudes.buyall.items.ScreenItems;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.List;

public class intro_activity extends AppCompatActivity {


    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter;
    TabLayout tabIndicator;
    Button nextBtn, getStarted;
    Animation btnAnim;

    int position = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if(restorePrefsData()){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();

        }

        setContentView(R.layout.activity_intro_activity);

        nextBtn = (Button) findViewById(R.id.btn_next_intro);
        btnAnim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_animation);
        getStarted = (Button) findViewById(R.id.getStartedBtn_intro);

        tabIndicator = findViewById(R.id.tab_indicator);

        final List<ScreenItems> mList = new ArrayList<>();

        mList.add(new ScreenItems("Buy or Provide The Products", "Compared to Tesla's third quarter, that's a healthy uptick in deliveries of 15,000 vehicles. The Tesla Model 3 remains the company's quickest-moving car; of the 112,000 cars delivered, 92,550 of them were Model 3s", R.drawable.backtonature));
        mList.add(new ScreenItems("Earn Decent Profit", "As is always the case, no financial figures were part of the announcement. Those will come during the automaker's Q4 earnings announcement at a later date.", R.drawable.slidethree));
        mList.add(new ScreenItems("We'll Deliver The Product for you", " This year, we should see the Model Y start production, which figures to boost production figures even higher as well. At the same time, Tesla is likely hoping to ride the wave of \"organic demand\" it cited in Q3 to continue posting strong Model 3 sales.", R.drawable.slidetwo));

        // Setup viewPager
        screenPager = findViewById(R.id.slide_intro_viewPager);
        introViewPagerAdapter = new IntroViewPagerAdapter(this, mList);
        screenPager.setAdapter(introViewPagerAdapter);


        // Setup TabLayout with  Viewpager
        tabIndicator.setupWithViewPager(screenPager);

        //Next Btn On click
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = screenPager.getCurrentItem();
                if (position < mList.size()) {
                    position++;
                    screenPager.setCurrentItem(position);
                }
                if (position == mList.size()-1) {

                    loadLastScreen();
                }

            }
        });


        //tablaout add change listener
        tabIndicator.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if  (tab.getPosition()==mList.size()-1){

                    loadLastScreen();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

                savePrefsData();
                finish();
            }
        });


    }

    private boolean restorePrefsData() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        Boolean isIntroActivityOpenedBefore = preferences.getBoolean("isIntroOpened", false);
        return isIntroActivityOpenedBefore;
    }

    private void savePrefsData() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putBoolean("isIntroOpened", true);
        editor.commit();
    }

    private void loadLastScreen() {
        nextBtn.setVisibility(View.INVISIBLE);
        getStarted.setVisibility(View.VISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);

        getStarted.setAnimation(btnAnim);
    }
}
